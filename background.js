console.log("Running Role switcher background");


var configItems;

function getConfig() {
    // Read it using the storage API
    chrome.storage.sync.get(['configJson'], function (items) {
        configItems = JSON.parse(items.configJson);
        console.log('Alibaba Roles Loaded ' + configItems.configs.length);
        var parent = chrome.contextMenus.create({ title: 'Awesome Alibaba Role Switcher'})
        for (let i in configItems.configs) {
            console.log(configItems.configs[i].alias);
            chrome.contextMenus.create({
                "title": configItems.configs[i].alias, "parentId": parent
            });
        }
    });
}

getConfig();


/* Register a listener for the `onClicked` event */
chrome.contextMenus.onClicked.addListener(function (info, tab) {
    if (tab) {
        /* Create the code to be injected */
        var menuIndex = info.menuItemId - 2;
        var code = [
            // `document.location.href= $('a[title="Switch Role"]')[0].href;`
             `document.getElementsByName("parentAlias")[0].value = "${configItems.configs[menuIndex].account}";`
            +`document.getElementsByName("roleName")[0].value = "${configItems.configs[menuIndex].roleName}";`
        ].join("\n");

        /* Inject the code into the current tab */
        chrome.tabs.executeScript(tab.id, { code: code });
    }
});