// Saves options to chrome.storage
function save_options() {
    var configJson = document.getElementById('configJson').value;
    chrome.storage.sync.set({
        configJson: configJson,
    }, function() {
        // Update status to let user know options were saved.
        var status = document.getElementById('statusOk');
        status.textContent = 'Options saved.';
        setTimeout(function() {
            status.textContent = '';
        }, 750);
    });
}

// Restores select box and checkbox state using the preferences
// stored in chrome.storage.
function restore_options() {
    // Use default value color = 'red' and likesColor = true.
    chrome.storage.sync.get({
        configJson: 'set me'
    }, function(items) {
        document.getElementById('configJson').value = items.configJson;
    });
}
document.addEventListener('DOMContentLoaded', restore_options);
document.getElementById('save').addEventListener('click', save_options);